package de.derandroidpro.aide.spinnerexample;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;

public class MainActivity extends Activity 
{
	Spinner s1, s2;
	
	boolean spinnerListener2Called;
	
	String[] s1Values = {"Android", "iOS"};
	String[] s2Values1 = {"5.0" , "5.1", "6.0"};
	String[] s2Values2 = {"iOS 5", "iOS 6", "iOS 7"};
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		
		s1 = (Spinner) findViewById(R.id.spinner1);
		
		ArrayAdapter spinner1Adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, s1Values );
		s1.setAdapter(spinner1Adapter);
		s1.setOnItemSelectedListener(new OnItemSelectedListener() {
			
			
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
										   int arg2, long arg3) {

								   
											   
					int spinnerposition = s1.getSelectedItemPosition();
					//Toast.makeText(getApplicationContext(), "Spinner Listener", Toast.LENGTH_SHORT).show();
					
					switch(spinnerposition){
						
						case 0:
							setupSpinner(s2Values1, s2);
							break;
							
						case 1:
							setupSpinner(s2Values2, s2);
							break;
						
					
				}

			}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}

			});
		
		s2 = (Spinner) findViewById(R.id.spinner2);
		
		
    }
	
	
	private void setupSpinner (final String[] svalues, final Spinner spinner){
		spinnerListener2Called = false;
		
		ArrayAdapter spinner2Adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, svalues );
		spinner.setAdapter(spinner2Adapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
								   int arg2, long arg3) {
					if(spinnerListener2Called){					   

					int spinnerposition = spinner.getSelectedItemPosition();
					Toast.makeText(getApplicationContext(), svalues[spinnerposition] + " wurde Ausgewählt.", Toast.LENGTH_SHORT).show();
	
						} else{
							spinnerListener2Called = true;
						}
						
					}
					
				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}

				});
	}		

	
}
